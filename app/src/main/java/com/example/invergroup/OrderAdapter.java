package com.example.invergroup;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.invergroup.utils.Constants;

import org.w3c.dom.Text;

import java.util.List;


public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    private final List<OrderModel> ordersList;


    public OrderAdapter(List<OrderModel> ordersList) {
        this.ordersList = ordersList;
    }

    @NonNull
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAdapter.ViewHolder holder, int position) {

        Context context = holder.itemView.getContext();
        String id = ordersList.get(position).getId().toString();
        String client = ordersList.get(position).getClient();
        String product = ordersList.get(position).getName();
        String quantity = ordersList.get(position).getQuantity().toString();
        String price = ordersList.get(position).getPrice().toString();
        String status = ordersList.get(position).getStatus();

        holder.id.setText(id);
        holder.client.setText(client);
        holder.product.setText(product);
        holder.quantity.setText(quantity);
        holder.price.setText(price);

        holder.itemView.setOnClickListener(v -> {
            NavController navController = Navigation.findNavController(holder.itemView);
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.OBJ_PEDIDO, ordersList.get(position));
            navController.navigate(R.id.orderDetailFragment, bundle);
        });

        switch (status){
            case "created":
                holder.status.setText("Creado");
                holder.img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_created));
                break;
            case "paid":
                holder.status.setText("Pagado");
                holder.img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_paid));
                break;
            case "verified":
                holder.status.setText("Verificado");
                holder.img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_verified));
                break;
            case "ready":
                holder.status.setText("Listo para recojo");
                holder.img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_ready));
                break;
            case "delivered":
                holder.status.setText("Entregado");
                holder.img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_delivered));
                break;
            case "canceled":
                holder.status.setText("Cancelado");
                holder.img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_status_canceled));
                break;
            default:
                break;
        }

    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView id, client, product, quantity, price, status;
        private final ImageView img;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.row_val_id);
            client = itemView.findViewById(R.id.row_val_client);
            product = itemView.findViewById(R.id.row_val_product);
            quantity = itemView.findViewById(R.id.row_val_quantity);
            price = itemView.findViewById(R.id.row_val_price);
            status = itemView.findViewById(R.id.row_val_status);
            img = itemView.findViewById(R.id.img_status_icon);

        }
    }
}

