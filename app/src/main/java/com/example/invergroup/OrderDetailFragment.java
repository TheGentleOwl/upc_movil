package com.example.invergroup;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.invergroup.utils.Constants;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

public class OrderDetailFragment extends Fragment {

    private OrderModel dOrder;

    public OrderDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dOrder = getArguments().getParcelable(Constants.OBJ_PEDIDO);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        NavController navController = Navigation.findNavController(view);

        ImageView productImg = view.findViewById(R.id.productImg);
        ImageView invoiceImg = view.findViewById(R.id.invoiceImg);
        TextView id = view.findViewById(R.id.dtl_val_id);
        TextView product = view.findViewById(R.id.dtl_val_product);
        TextView description = view.findViewById(R.id.dtl_val_desc);
        TextView client = view.findViewById(R.id.dtl_val_client);
        TextView quantity = view.findViewById(R.id.dtl_val_quantity);
        TextView price = view.findViewById(R.id.dtl_val_price);
        TextView status = view.findViewById(R.id.dtl_val_status);
        Button set_verified = view.findViewById(R.id.dtl_btn_verify);
        Button set_ready = view.findViewById(R.id.dtl_btn_ready);
        Button set_delivered = view.findViewById(R.id.dtl_btn_deliver);

        Glide.with(view)
                .load(Constants.API_BASE + dOrder.getImage())
                .centerCrop()
                .placeholder(R.drawable.ic_wait)
                .into(productImg);

        Glide.with(view)
                .load(Constants.API_BASE + dOrder.getInvoice())
                .centerCrop()
                .placeholder(R.drawable.ic_wait)
                .into(invoiceImg);

        id.setText(dOrder.getId().toString());
        product.setText(dOrder.getName());
        description.setText(dOrder.getDescription());
        client.setText(dOrder.getClient());
        quantity.setText(dOrder.getQuantity().toString());
        price.setText(dOrder.getPrice().toString());
        status.setText(dOrder.getStatus());

        set_verified.setOnClickListener(v -> {
            update_status(dOrder.getId(), "verified", navController);
        });
        set_ready.setOnClickListener(v -> {
            update_status(dOrder.getId(), "ready", navController);
        });
        set_delivered.setOnClickListener(v -> {
            update_status(dOrder.getId(), "delivered", navController);
        });

    }

    private void update_status(int id, String status, NavController navController) {
        String url = Constants.API_ORDERS + String.valueOf(id);
        Log.i("===",url);
        StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        navController.popBackStack();

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", String.valueOf(error));
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("status", status);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getContext());
        queue.add(putRequest);
    }

}