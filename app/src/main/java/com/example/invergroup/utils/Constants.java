package com.example.invergroup.utils;

public class Constants {
    public static final String OBJ_PEDIDO = "objPedido";
    public static final String API_BASE = "https://8004-2800-200-e630-13a0-709a-2497-9e62-2145.ngrok.io";
    public static final String API_ORDERS = API_BASE + "/api/orders/";

}
