package com.example.invergroup;

import android.os.Parcel;
import android.os.Parcelable;

public class OrderModel implements Parcelable {
    private Integer id;
    private String client;
    private Integer code;
    private String name;
    private String description;
    private Double price;
    private String image;
    private Integer quantity;
    private String pickup_at;
    private String status;
    private String invoice;


    public OrderModel(Integer id, String client, Integer code,
                 String name, String description, Double price, String image,
                 Integer quantity, String pickup_at, String status, String invoice) {
        this.id = id;
        this.client = client;
        this.code = code;
        this.name = name;
        this.description = description;
        this.price = price;
        this.image = image;
        this.quantity = quantity;
        this.pickup_at = pickup_at;
        this.status = status;
        this.invoice = invoice;

    }

    protected OrderModel(Parcel parcel) {
        if (parcel.readByte() == 0) {
            id = null;
        } else {
            id = parcel.readInt();
        }
        client = parcel.readString();
        if (parcel.readByte() == 0) {
            code = null;
        } else {
            code = parcel.readInt();
        }
        name = parcel.readString();
        description = parcel.readString();
        if (parcel.readByte() == 0) {
            price = null;
        } else {
            price = parcel.readDouble();
        }
        image = parcel.readString();
        if (parcel.readByte() == 0) {
            quantity = null;
        } else {
            quantity = parcel.readInt();
        }
        pickup_at = parcel.readString();
        status = parcel.readString();
        invoice = parcel.readString();
    }

    public static final Creator<OrderModel> CREATOR = new Creator<OrderModel>() {
        @Override
        public OrderModel createFromParcel(Parcel in) {
            return new OrderModel(in);
        }

        @Override
        public OrderModel[] newArray(int size) {
            return new OrderModel[size];
        }
    };

    public Integer getId() {
        return id;
    }
    public String getClient() {
        return client;
    }
    public void setClient(String client) {
        this.client = client;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getPickup_at() {
        return pickup_at;
    }

    public void setPickup_at(String pickup_at) {
        this.pickup_at = pickup_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.client);
        parcel.writeInt(this.code);
        parcel.writeString(this.name);
        parcel.writeString(this.description);
        parcel.writeInt(this.quantity);
        parcel.writeDouble(this.price);
        parcel.writeString(this.image);
        parcel.writeString(this.status);
        parcel.writeString(this.pickup_at);
        parcel.writeString(this.invoice);
    }
}
