package com.example.invergroup.recyclerview;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.invergroup.MainActivity;
import com.example.invergroup.OrderAdapter;
import com.example.invergroup.OrderModel;
import com.example.invergroup.R;
import com.example.invergroup.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OrderListFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_orderlist, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerPedidos);
        getData(requireContext(), recyclerView);

    }

    private void getData(Context context, RecyclerView recyclerView) {
        List<OrderModel> list = new ArrayList<>();

        String url = Constants.API_ORDERS;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    for (int i=0; i<jsonArray.length(); i++){
                        JSONObject object = jsonArray.getJSONObject(i);
                        OrderModel order = new OrderModel(
                                object.getInt("id"),
                                object.getString("client"),
                                object.getInt("code"),
                                object.getString("name"),
                                object.getString("description"),
                                object.getDouble("price"),
                                object.getString("image"),
                                object.getInt("quantity"),
                                object.getString("pickup_at"),
                                object.getString("status"),
                                object.getString("invoice")
                        );

                        list.add(order);
                    }
                    RecyclerView.Adapter<OrderAdapter.ViewHolder> adapter = new OrderAdapter(list);
                    recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    recyclerView.setAdapter(adapter);
                } catch (JSONException e) {
                    Log.i("======>", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue queue = Volley.newRequestQueue(getContext());
        queue.add(stringRequest);
    }
}